
from fastapi import APIRouter
from fastapi import Depends, Path, Query
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.movie import Movie as MovieModel 
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()

#Permite consultar informacion          (3)
#En este caso muestra el listado de las peliculas que estan guardadas
@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
#    result = db.query(MovieModel).all()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#Permite consultar informacion          (5)
#Busqueda de peliculas por id
@movie_router.get('/movies/{id}', tags=['modificacion'], response_model=Movie, dependencies=[Depends(JWTBearer())])
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie:
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'message': 'No encontrado'})
#    for item in movies:
#        if item["id"] == id:
#            return JSONResponse(content=item)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#Permite consultar informacion          (8)
#Busqueda de peliculas por Categoria
@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie], dependencies=[Depends(JWTBearer())])       
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)) -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movie_by_category(category)
#    result = db.query(MovieModel).filter(MovieModel.category == category).all()
    if not result: 
        return JSONResponse(status_code = 404, content={'message':'No encontrado'})
#   data = [ item for item in movies if item['category'] == category ]
    return JSONResponse(content=jsonable_encoder(result))

#Permite crear un nuevo recurso         (4)
@movie_router.post('/movies', tags=['modificacion'], response_model=dict, status_code=201,dependencies=[Depends(JWTBearer())])
def create_movie(movie: Movie) -> dict:
    db = Session()   #Se crea una sesion para conectarse a la base de datos
    MovieService(db).create_movie(movie)
#    new_movie = MovieModel(**movie.dict())
#    db.add(new_movie)
#    db.commit()      #Se hace una actualizacion para que los datos se guarden 
    #movies.append(movie)
    return JSONResponse(status_code=201, content={"message": "Se ha registrado la película"})

#Permite modificar un recurso           (6)
@movie_router.put('/movies/{id}', tags=['modificacion'], response_model=dict, status_code=200,dependencies=[Depends(JWTBearer())])
def update_movie(id: int, movie: Movie)-> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
#    result = db.query(MovieModel).filter(MovieModel.id == id).first() 
    if not result:
        return JSONResponse(status_code = 404, content={'message':'No encontrado'})
    MovieService(db).update_movie(id, movie)
#    result.title = movie.title
#    result.overview = movie.overview
#    result.year = movie.year
#    result.rating = movie.rating
#    result.category = movie.category
#    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha modificado la película"})
#    for item in movies:
#		if item["id"] == id:
#			item['title'] = movie.title
#			item['overview'] = movie.overview
#			item['year'] = movie.year
#			item['rating'] = movie.rating
#			item['category'] = movie.category
	

#Permite eliminar un recurso            (7)
@movie_router.delete('/movies/{id}', tags=['modificacion'], response_model=dict, status_code=200,dependencies=[Depends(JWTBearer())])
def delete_movie(id: int)-> dict:
    db = Session()
    result: MovieModel = db.query(MovieModel).filter(MovieModel.id == id).first()
#    result = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'message': 'No enconrado'})
    MovieService(db).delete_movie(id)

#    db.delete(result)
#    db.commit()

#    for item in movies:
#        if item["id"] == id:
#            movies.remove(item)
    return JSONResponse(status_code=200, content={"message": "Se ha eliminado la película"})